# World First bank system #
========================

## Prerequisites ##
* PHP 5.5.24 or greater (The PHP timezone (date.timezone) must be set for Symfony console to function correctly)
* Composer (https://getcomposer.org/)
* git
* PHP Unit (4.7.0 is compatible with PHP 5.5.24)

## Installation ##
### Clone the repository ###

```
#!bash
git clone https://jcooke22@bitbucket.org/jcooke22/world-first-bank.git
cd world-first-bank
```

### Install the dependencies ###

```
#!bash
composer install
#The Symfony install script will ask for some parameter values "Some parameters are missing. Please provide them.". These are not important for this project, just hit enter on each to use the default values.
```

### Initialise the DB ###

```
#!bash
php app/console doctrine:database:create
php app/console doctrine:schema:update --force
```

## Running the bank system ##
In the following examples the account number is 1

### Open account ###

```
#!bash
php app/console bank:open-account "James Cooke"
```

### Request overdraft ###

```
#!bash
php app/console bank:request-overdraft 1 1000.00
```

### Deposit ###

```
#!bash
php app/console bank:deposit 1 150.25
```

### Withdraw ###

```
#!bash
php app/console bank:withdraw 1 50.00
```

### Request balance ###

```
#!bash
php app/console bank:request-balance 1
```

### Close account ###

```
#!bash
php app/console bank:close-account 1
```


### Unit tests ###

All commands in the system have tests however not all will pass by default. The input parameters for the command unit tests are held in JSON in src/AppBundle/Tests/data/***.json. This test data may be modified to suit the testing requirements.

```
#!bash
# To run all tests
phpunit -c app/
```