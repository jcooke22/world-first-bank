<?php
use AppBundle\Command;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

use AppBundle\Tests\Command as TestCommand;
/**
 * Class BankCloseAccountCommandTest
 */
class BankCloseAccountCommandTest extends KernelTestCase
{
    /**
     * @dataProvider testCloseAccountProvider
     */
    public function testCloseAccount($accountNumber, $accountHolderName, $amount, $expectedString)
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new Command\BankCloseAccountCommand());
        
        $command = $application->find('bank:close-account');
        
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            'account-number' => $accountNumber
        ));
        
        $this->assertRegExp('/' . $expectedString . '/', $commandTester->getDisplay());
    }

    /**
     * Data provider for the test
     *
     * @author James Cooke
     * @return array
     */
    public function testCloseAccountProvider()
    {
        return TestCommand\TestDataLoader::loadData('./src/AppBundle/Tests/data/testCloseAccount.json');
    }

}