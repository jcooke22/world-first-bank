<?php
use AppBundle\Command;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

use AppBundle\Tests\Command as TestCommand;
/**
 * Class BankWithdrawCommandTest
 */
class BankWithdrawCommandTest extends KernelTestCase
{
    /**
     * @dataProvider testWithdrawProvider
     */
    public function testWithdraw($accountNumber, $accountHolderName, $amount, $expectedString)
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new Command\BankWithdrawCommand());
        
        $command = $application->find('bank:withdraw');
        
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            'account-number' => $accountNumber,
            'amount'         => $amount
        ));
        
        $this->assertRegExp('/' . $expectedString . '/', $commandTester->getDisplay());
    }

    /**
     * Data provider for the test
     *
     * @author James Cooke
     * @return array
     */
    public function testWithdrawProvider()
    {
        return TestCommand\TestDataLoader::loadData('./src/AppBundle/Tests/data/testWithdraw.json');
    }

}