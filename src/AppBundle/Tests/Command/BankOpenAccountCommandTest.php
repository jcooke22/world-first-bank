<?php
use AppBundle\Command;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

use AppBundle\Tests\Command as TestCommand;
/**
 * Class BankOpenAccountCommandTest
 */
class BankOpenAccountCommandTest extends KernelTestCase
{
    /**
     * @dataProvider testOpenAccountProvider
     */
    public function testOpenAccount($accountNumber, $accountHolderName, $amount, $expectedString)
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new Command\BankOpenAccountCommand());
        
        $command = $application->find('bank:open-account');

        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            'account-holder-name' => $accountHolderName
        ));
        
        $this->assertRegExp('/' . $expectedString . '/', $commandTester->getDisplay());
    }

    /**
     * Data provider for the test
     *
     * @author James Cooke
     * @return array
     */
    public function testOpenAccountProvider()
    {
        return TestCommand\TestDataLoader::loadData('./src/AppBundle/Tests/data/testOpenAccount.json');
    }

}