<?php
use AppBundle\Command;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

use AppBundle\Tests\Command as TestCommand;
/**
 * Class BankRequestOverdraftCommandTest
 */
class BankRequestOverdraftCommandTest extends KernelTestCase
{
    /**
     * @dataProvider testRequestOverdraftProvider
     */
    public function testRequestOverdraft($accountNumber, $accountHolderName, $amount, $expectedString)
    {
        $kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new Command\BankRequestOverdraftCommand());
        
        $command = $application->find('bank:request-overdraft');
        
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            'account-number' => $accountNumber,
            'amount'         => $amount
        ));
        
        $this->assertRegExp('/' . $expectedString . '/', $commandTester->getDisplay());
    }

    /**
     * Data provider for the test
     *
     * @author James Cooke
     * @return array
     */
    public function testRequestOverdraftProvider()
    {
        return TestCommand\TestDataLoader::loadData('./src/AppBundle/Tests/data/testRequestOverdraft.json');
    }

}