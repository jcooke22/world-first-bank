<?php
namespace AppBundle\Tests\Command;

/**
 * Class TestDataLoader
 */
class TestDataLoader
{
    public static function loadData($filePath)
    {
        if (!file_exists($filePath)) {
            return array();
        }
        $dataObj =json_decode(file_get_contents($filePath));
        $dataArr = array();
        foreach ($dataObj as $dataObjItem) {
            $dataArrItem = array();
            $dataArrItem[] = $dataObjItem->accountNumber;
            $dataArrItem[] = $dataObjItem->accountHolderName;
            $dataArrItem[] = $dataObjItem->amount;
            $dataArrItem[] = $dataObjItem->expectedString;
            $dataArr[] = $dataArrItem;
        }
        return $dataArr;
    }

}