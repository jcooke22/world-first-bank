<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\FileLocator;


/**
 * Class BankCloseAccountCommand
 * @package AppBundle\Command
 */
class BankCloseAccountCommand extends ContainerAwareCommand
{
    private $container;

    /**
     * Set up the command
     * @author James Cooke
     */
    protected function configure()
    {
        $this
            ->setName('bank:close-account')
            ->setDescription('Close a bank account')
            ->addArgument(
                'account-number',
                InputArgument::REQUIRED,
                'What is the account number'
            )
        ;
    }

    /**
     * Run the command
     *
     * @author James Cooke
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get the container
        $this->container = $this->getApplication()->getKernel()->getContainer();

        // Get the account number
        $accountNumber = $input->getArgument('account-number');
        
        // Find the account
        $em = $this->getContainer()->get('doctrine')->getManager();
        $account = $em
            ->getRepository('AppBundle:Account')
            ->find($accountNumber);
        
        // Account not found
        if (!$account) {
            $output->writeln(sprintf('<error>Account number %s not found.</error>', $accountNumber));
            return false;
        }
        
        // Check if the account is already closed
        if ($account && $account->isClosed()) {
            $output->writeln(sprintf('<error>Account number %s is already closed.</error>', $accountNumber));
            return false;
        }
        
        // Check if the account is closable
        if ($account && !$account->isCloseable()) {
            $message = $account->getNotCloseableMessage() ? $account->getNotCloseableMessage() : 'Error: cannot close account';
            $output->writeln(sprintf('<error>%s</error>', $message));
            return false;
        }
        
        // Close the account
        if ($account && $account->isCloseable()) {
            $account->setStatus('closed');
            $em->persist($account);
            $em->flush();
            $output->writeln(sprintf('<info>Account number %s closed.</info>', $accountNumber));            
        }
    }
}