<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\FileLocator;

use AppBundle\Exception;

/**
 * Class BankWithdrawCommand
 * @package AppBundle\Command
 */
class BankWithdrawCommand extends ContainerAwareCommand
{
    private $container;

    /**
     * Set up the command
     * @author James Cooke
     */
    protected function configure()
    {
        $this
            ->setName('bank:withdraw')
            ->setDescription('Make a withdrawal from an account')
            ->addArgument(
                'account-number',
                InputArgument::REQUIRED,
                'What is the account number'
            )
            ->addArgument(
                'amount',
                InputArgument::REQUIRED,
                'What is the amount'
            )
        ;
    }

    /**
     * Run the command
     *
     * @author James Cooke
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get the container
        $this->container = $this->getApplication()->getKernel()->getContainer();

        // Get the account number
        $accountNumber = $input->getArgument('account-number');
        
        // Get the amount
        $amount = $input->getArgument('amount');

        // Convert the amount to an integer
        $amount = $amount * 100;

        // Find the account
        $em = $this->getContainer()->get('doctrine')->getManager();
        $account = $em
            ->getRepository('AppBundle:Account')
            ->find($accountNumber);

        // Account not found
        if (!$account) {
            $output->writeln(sprintf('<error>Account number %s not found.</error>', $accountNumber));
            return false;
        }

        // Check if the account is closed
        if ($account && $account->isClosed()) {
            $output->writeln(sprintf('<error>Account number %s is closed.</error>', $accountNumber));
            return false;
        }

        if ($account && !$account->isClosed()) {
            try {
                // Make the withdrawal
                $account->withdraw($amount);

                // Persist to DB
                $em->persist($account);
                $em->flush();
                $output->writeln(sprintf('<info>Withdrawal Successful for account number %s.</info>', $accountNumber));
                $output->writeln(sprintf('<info>New balance is %s</info>', $account->getBalanceFormatted()));
            } catch (Exception\BankException $e) {
                // Exception
                $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
            }
        }
    }
}