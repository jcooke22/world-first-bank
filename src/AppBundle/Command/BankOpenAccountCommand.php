<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\FileLocator;

use AppBundle\Entity;

/**
 * Class BankOpenAccountCommand
 * @package AppBundle\Command
 */
class BankOpenAccountCommand extends ContainerAwareCommand
{
    private $container;

    /**
     * Set up the command
     * @author James Cooke
     */
    protected function configure()
    {
        $this
            ->setName('bank:open-account')
            ->setDescription('Open a bank account')
            ->addArgument(
                'account-holder-name',
                InputArgument::REQUIRED,
                'What is the full name of the account holder'
            )
        ;
    }

    /**
     * Run the command
     *
     * @author James Cooke
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get the container
        $this->container = $this->getApplication()->getKernel()->getContainer();

        // Get the account holder name
        $accountHolderName = $input->getArgument('account-holder-name');

        // Create the account
        $account = new Entity\Account();
        $account->setStatus('open');
        $account->setAccountHolderName($accountHolderName);
        $account->setBalance(0);
        $account->setOverdraftLimit(0);

        // Persist this to the DB
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->persist($account);
        $em->flush();

        // Get the account number
        $accountNumber = $account->getId();
        
        // Write the output
        $output->writeln(sprintf('<info>Account opened for %s. Account number: %s.</info>', $accountHolderName, $accountNumber));

    }
}