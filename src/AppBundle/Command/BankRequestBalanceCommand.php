<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Config\FileLocator;


/**
 * Class BankRequestBalanceCommand
 * @package AppBundle\Command
 */
class BankRequestBalanceCommand extends ContainerAwareCommand
{
    private $container;

    /**
     * Set up the command
     * @author James Cooke
     */
    protected function configure()
    {
        $this
            ->setName('bank:request-balance')
            ->setDescription('Request an account balance')
            ->addArgument(
                'account-number',
                InputArgument::REQUIRED,
                'What is the account number'
            )
        ;
    }

    /**
     * Run the command
     *
     * @author James Cooke
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get the container
        $this->container = $this->getApplication()->getKernel()->getContainer();

        // Get the account number
        $accountNumber = $input->getArgument('account-number');

        // Find the account
        $em = $this->getContainer()->get('doctrine')->getManager();
        $account = $em
            ->getRepository('AppBundle:Account')
            ->find($accountNumber);

        // Account not found
        if (!$account) {
            $output->writeln(sprintf('<error>Account number %s not found.</error>', $accountNumber));
            return false;
        }

        // Check if the account is closed
        if ($account && $account->isClosed()) {
            $output->writeln(sprintf('<error>Account number %s is closed.</error>', $accountNumber));
            return false;
        }

        // Write the balance to the output
        if ($account) {
            $output->writeln(sprintf('<info>Hello %s.</info>', $account->getAccountHolderName()));
            $output->writeln(sprintf('<info>Balance for account number %s is %s.</info>', $accountNumber, $account->getBalanceFormatted()));
            if ($account->hasOverdraftFacility()) {
                $output->writeln(sprintf('<info>Account overdraft limit is %s.</info>', $account->getOverdraftLimitFormatted()));
            }
            $output->writeln(sprintf('<info>Available funds are %s.</info>', $account->getAvailableFundsFormatted()));
            return true;
        }
    }
}