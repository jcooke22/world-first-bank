<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Exception;

/**
* @ORM\Entity
* @ORM\Table(name="account")
*/
class Account
{
    /**
     * The maximum value for an overdraft facility
     * @todo - Get this from config
     * @var int
     */
    const MAX_OVERDRAFT_VALUE = 150000;
    
    /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;
    
    /**
    * @ORM\Column(type="string", length=100)
    */
    protected $accountHolderName;
    
    /**
    * @ORM\Column(type="integer")
    */
    protected $balance;

    /**
     * @ORM\Column(type="integer")
     */
    protected $overdraftLimit;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $status;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountHolderName
     *
     * @param string $accountHolderName
     *
     * @return Account
     */
    public function setAccountHolderName($accountHolderName)
    {
        $this->accountHolderName = $accountHolderName;

        return $this;
    }

    /**
     * Get accountHolderName
     *
     * @return string
     */
    public function getAccountHolderName()
    {
        return $this->accountHolderName;
    }

    /**
     * Set balance
     *
     * @param integer $balance
     *
     * @return Account
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return integer
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set overdraftLimit
     *
     * @param integer $overdraftLimit
     *
     * @return Account
     */
    public function setOverdraftLimit($overdraftLimit)
    {
        $this->overdraftLimit = $overdraftLimit;

        return $this;
    }

    /**
     * Get overdraftLimit
     *
     * @return integer
     */
    public function getOverdraftLimit()
    {
        return $this->overdraftLimit;
    }
    
    /**
     * Set status
     *
     * @param string $status
     *
     * @return Account
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Is the account closed
     *
     * @author James Cooke
     * @return bool
     */
    public function isClosed()
    {
        return $this->getStatus() == 'closed';
    }

    /**
     * Check whether or not the account is closable
     *
     * @author James Cooke
     * @return bool
     */
    public function isCloseable()
    {
        return $this->getBalance() == 0;
    }
    
    /**
     * Get the non closable message 
     *
     * @author James Cooke
     * @return String|null
     */
    public function getNotCloseableMessage()
    {
        if ($this->isCloseable()) {
            return null;
        }
        if ($this->getBalance() < 0) {
            return 'This account is in debt. Please deposit ' . $this->_formatMoney(abs($this->getBalance())) . ' before closing the account';
        }
        if ($this->getBalance() > 0) {
            return 'This account is in credit. Please withdraw ' . $this->_formatMoney($this->getBalance()) . ' before closing the account';
        }
    }

    /**
     * Request an overdraft limit change
     * @author James Cooke
     * @param $requestedLimit
     * @return bool
     */
    public function requestOverdraft($requestedLimit)
    {
        if ($requestedLimit < 0) {
            throw new Exception\BankException('Overdraft limit must be a positive value');
        }
        if ($requestedLimit > self::MAX_OVERDRAFT_VALUE) {
            throw new Exception\BankException('Overdraft facility cannot exceed ' . $this->_formatMoney(self::MAX_OVERDRAFT_VALUE)); 
        }
        $this->setOverdraftLimit($requestedLimit);
        return true;
    }

    /**
     * Get the available funds for the account
     *
     * @author  James Cooke
     */
    public function getAvailableFunds()
    {
        return $this->getBalance() + $this->getOverdraftLimit();
    }
    
    /**
     * Get the available funds for the account formatted
     *
     * @author  James Cooke
     */
    public function getAvailableFundsFormatted()
    {
        return $this->_formatMoney($this->getAvailableFunds());
    }

    /**
     * Make a deposit
     *
     * @author  James Cooke
     * @param $amount
     * @return bool
     */
    public function deposit($amount)
    {
        $this->setBalance($this->getBalance() + $amount);
        return true;
    }
    
    /**
     * Make a withdrawal
     *
     * @author  James Cooke
     * @param $amount
     * @return bool
     */
    public function withdraw($amount)
    {
        // Check if there is enough available funds
        if ($this->getAvailableFunds() < $amount) {
            throw new Exception\BankException('Sorry, insufficient funds');
        }
        
        $this->setBalance($this->getBalance() - $amount);
        return true;
    }

    /**
     * Get a formatted balance
     *
     * @author James Cooke
     * @return string
     */
    public function getBalanceFormatted()
    {
        return $this->_formatMoney($this->getBalance());
    }
    
    /**
     * Get a formatted overdraft limit
     *
     * @author James Cooke
     * @return string
     */
    public function getOverdraftLimitFormatted()
    {
        return $this->_formatMoney($this->getOverdraftLimit());
    }

    /**
     * Define whether or not the account has an overdraft facility
     *
     * @author James Cooke
     * @return bool
     */
    public function hasOverdraftFacility()
    {
        return $this->getOverdraftLimit() > 0;
    }

    /**
     * Format a value as money
     *
     * @author James Cooke
     * @param $value
     * @return string
     */
    private function _formatMoney($value)
    {
        // Set the currency to GPB
        // @todo - Read this from a config
        setlocale(LC_MONETARY, 'en_GB');
        
        // Handle positive values
        if ($value >= 0) {
            return money_format('%n', $value / 100);
        }
        
        // Handle negative values
        if ($value < 0) {
            $absValueFormatted = $this->_formatMoney(abs($value));
            return '-' . $absValueFormatted;
        }
    }
}
