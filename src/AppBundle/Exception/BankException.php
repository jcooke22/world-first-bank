<?php

namespace AppBundle\Exception;


use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class BankException
 * @package AppBundle\Exception
 */
class BankException extends Exception
{
    public function __construct($message)
    {
        $this->message = $message; 
        parent::__construct($this->getMessage(), null, null);
    }
}
